﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Configuration;
using BsmEcommerceVentas.bsmService;
using System.ServiceModel;
using Janus.Windows.GridEX;

namespace BsmEcommerceVentas
{
    public partial class frmMain : Form
    {
        string _urlWs = ConfigurationManager.AppSettings["UrlWs"];
        string _sistemaPK = ConfigurationManager.AppSettings["SistemaPK"];
        string _empresaPK = ConfigurationManager.AppSettings["EmpresaPK"];
        string _turnoPK = ConfigurationManager.AppSettings["TurnoPK"];
        string _vendedorPK = ConfigurationManager.AppSettings["VendedorPK"];
        string _sucursalPK = ConfigurationManager.AppSettings["SucursalPK"];
        string _horaOrigenPK = ConfigurationManager.AppSettings["HoraorigenPK"];
        string _usuarioOrigenPK = ConfigurationManager.AppSettings["UsuarioOrigenPK"];
        string _usuarioPedPK = ConfigurationManager.AppSettings["UsuarioPedPK"];
        string _clientePK = ConfigurationManager.AppSettings["ClintePK"];
        string _sucHastaClasePK = ConfigurationManager.AppSettings["SucHastaClasePK"];
        string _sucHastaPK = ConfigurationManager.AppSettings["SucHastaPK"];
        string _costoPK = ConfigurationManager.AppSettings["CostoPK"];
        string _indiceDetallePK = ConfigurationManager.AppSettings["IndiceDetallePK"];
        string _proveedorPK = ConfigurationManager.AppSettings["ProveedorPK"];
        string _chequePK = ConfigurationManager.AppSettings["ChequePK"];
        string _valorChequePK = ConfigurationManager.AppSettings["ValorChequePK"];
        string _indiceChequePK = ConfigurationManager.AppSettings["IndiceChequePK"];
        string _sucursalChequePK = ConfigurationManager.AppSettings["SucursalChequePK"];
        string _sucursalChequeCvlPK = ConfigurationManager.AppSettings["SucursalChequeCvlPK"];
        string _nombreClientePK = ConfigurationManager.AppSettings["NombreClientePK"];
        string _fechaClientePK = ConfigurationManager.AppSettings["FechaAltaClintePK"];
        string _domicilioClientePK = ConfigurationManager.AppSettings["DomicilioClientePK"];
        string _postalClientePK = ConfigurationManager.AppSettings["PostalClientePK"];
        string _cPostalClientePK = ConfigurationManager.AppSettings["cPostalClientePK"];
        string _provinciaClientePK = ConfigurationManager.AppSettings["ProvinciaClientePK"];
        string _tipoIvaClientePK = ConfigurationManager.AppSettings["TipoIvaClientePK"];
        string _condPagoClientePK = ConfigurationManager.AppSettings["CondPagoClientePK"];
        string _tipoDocClientePK = ConfigurationManager.AppSettings["TipoDocClientePK"];
        string _codigoClientePK = ConfigurationManager.AppSettings["CodigoClientePK"];
        string _vendedorClientePK = ConfigurationManager.AppSettings["VendedorClientePK"];
        string _formaPagoClientePK = ConfigurationManager.AppSettings["FormaPagoClientePK"];
        string _cuitClientePK = ConfigurationManager.AppSettings["CuitClintePK"];
        string _personaNomClientePK = ConfigurationManager.AppSettings["PersonaNomClientePK"];
        string _personaApeClientePK = ConfigurationManager.AppSettings["PeresonaApeClientePK"];
        string _personaGeneClientePK = ConfigurationManager.AppSettings["PersonaGeneClientePK"];
        string _actualizaClientePK = ConfigurationManager.AppSettings["ActualizaClientePK"];
        string _skuEnvio = ConfigurationManager.AppSettings["SkuEnvio"];
        string _skuDescuento = ConfigurationManager.AppSettings["SkuDescuento"];


        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

        }

        private void btnBuscarExcel_Click(object sender, EventArgs e)
        {
            //creamos un objeto OpenDialog que es un cuadro de dialogo para buscar archivos
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Archivos de Excel (*.xls;*.xlsx)|*.xls;*.xlsx"; //le indicamos el tipo de filtro en este caso que busque
            //solo los archivos excel

            dialog.Title = "Seleccione el archivo de Excel";//le damos un titulo a la ventana

            dialog.FileName = string.Empty;//inicializamos con vacio el nombre del archivo

            //si al seleccionar el archivo damos Ok
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //el nombre del archivo sera asignado al textbox
                txtArchivo.Text = dialog.FileName;
            }
        }

        private void btnImportar_Click(object sender, EventArgs e)
        {
            string hoja = txtHoja.Text;
            string archivo = txtArchivo.Text;
            //Vemos si tenemos algo en el archivo y en la hoja
            if (!string.IsNullOrEmpty(archivo))
            {
                if (!string.IsNullOrEmpty(hoja))
                {
                    //declaramos las variables         
                    OleDbConnection conexion = null;
                    //DataSet dataSet = null;
                    OleDbDataAdapter dataAdapter = null;
                    string consultaHojaExcel = "Select * from [" + hoja + "$]";

                    //esta cadena es para archivos excel 2007 y 2010
                    string cadenaConexionArchivoExcel = "provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + archivo + "';Extended Properties=Excel 12.0;";

                    //para archivos de 97-2003 usar la siguiente cadena
                    //string cadenaConexionArchivoExcel = "provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + archivo + "';Extended Properties=Excel 8.0;";

                    try
                    {
                        //Si el usuario escribio el nombre de la hoja se procedera con la busqueda
                        conexion = new OleDbConnection(cadenaConexionArchivoExcel);//creamos la conexion con la hoja de excel
                        conexion.Open(); //abrimos la conexion
                        dataAdapter = new OleDbDataAdapter(consultaHojaExcel, conexion); //traemos los datos de la hoja y las guardamos en un dataSdapter
                        //dataSet = new DataSet(); // creamos la instancia del objeto DataSet
                        dataAdapter.Fill(dts, hoja);//llenamos el dataset
                        //dataGridView1.DataSource = dataSet.Tables[0]; //le asignamos al DataGridView el contenido del dataSet
                        conexion.Close();//cerramos la conexion
                        //Seteamos los datos en el grid.
                        grExcel.SetDataBinding(dts, hoja);
                        grExcel.RetrieveStructure();
                        //Vemos si tenemos algo en el grid.
                        if (grExcel.RowCount > 0)
                        {
                            pnlIngresar.Enabled = true;
                            //pnlGrid.Enabled = true;
                        }
                        else
                        {
                            pnlIngresar.Enabled = false;
                            //pnlGrid.Enabled = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        //en caso de haber una excepcion que nos mande un mensaje de error
                        MessageBox.Show("Error, Verificar el archivo o el nombre de la hoja", ex.Message);
                    }
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("Debe ingresar la hoja del archivo.",
                                                    "", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                }
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Debe seleccionar un archivo Excel",
                                 "", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
        }

        private void btnIngresarSubfolder_Click(object sender, EventArgs e)
        {
            List<ErrorProcess> _lstError = new List<ErrorProcess>();
            List <Cabecera> _lstCabecera = new List<Cabecera>();
            List<Detalle> _lstDetalle = new List<Detalle>();
            List<Pago> _lstPago = new List<Pago>();
            List<Cliente> _lstCliente = new List<Cliente>();
            try
            {
                //Recorro el grid y Cargo los datos en las listas.
                foreach (Janus.Windows.GridEX.GridEXRow grw in grExcel.GetRows())
                {
                    string _comprobante = grw.Cells["Comprobante"].Value.ToString();
                    //Vemos si existe el comprobante en la cabecera.
                    var header = _lstCabecera.Find(h => h.Comprobante == _comprobante);

                    if (header == null)
                    {
                        CargoCabecera(grw, _comprobante, _lstCabecera);
                    }

                    //Veo si tengo descuento.
                    if (Convert.ToDecimal(grw.Cells["Descuento"].Value) > 0)
                    {
                        var _dto = _lstDetalle.Find(d => d.Articulo == _skuDescuento && d.comprobante == _comprobante);
                        if (_dto == null)
                        {
                            CargoDescuento(grw, _comprobante, _lstDetalle);
                        }
                    }

                    //Vemos si tenemos el envio.
                    if(Convert.ToDecimal(grw.Cells["Envio"].Value) > 0)
                    {
                        var envio = _lstDetalle.Find(en => en.Articulo == _skuEnvio && en.comprobante == _comprobante);
                        if(envio == null)
                        {
                            CargoEnvio(grw, _comprobante, _lstDetalle);
                        }
                    }

                    //Detalle.               
                    Detalle _det = new Detalle();
                    _det.comprobante = _comprobante;
                    _det.Articulo = grw.Cells["Articulo"].Value.ToString();
                    _det.Cantidad = Convert.ToDecimal(grw.Cells["Cantidad"].Value, System.Globalization.CultureInfo.InvariantCulture);
                    _det.ClaseArt = grw.Cells["ClaseArt"].Value.ToString();
                    _det.Color = grw.Cells["ColorNom"].Value.ToString();
                    _det.Columna = Convert.ToInt32(grw.Cells["Columna"].Value);
                    _det.CondPago = Convert.ToInt32(grw.Cells["CondPagoDetalle"].Value);
                    _det.Costo = Convert.ToDecimal(_costoPK, System.Globalization.CultureInfo.InvariantCulture);
                    _det.Descrip = grw.Cells["Descrip"].Value.ToString();
                    _det.Dto1 = grw.Cells["Dto1"].Value == DBNull.Value ? 0 : Convert.ToDecimal(grw.Cells["Dto1"].Value, System.Globalization.CultureInfo.InvariantCulture);
                    _det.Dto2 = grw.Cells["Dto2"].Value == DBNull.Value ? 0 : Convert.ToDecimal(grw.Cells["Dto2"].Value, System.Globalization.CultureInfo.InvariantCulture);
                    _det.Dto3 = grw.Cells["Dto3"].Value == DBNull.Value ? 0 : Convert.ToDecimal(grw.Cells["Dto3"].Value, System.Globalization.CultureInfo.InvariantCulture);
                    _det.Indice = _indiceDetallePK;
                    _det.IvaPorc = grw.Cells["IvaPorc"].Value == DBNull.Value ? 0 : Convert.ToDecimal(grw.Cells["IvaPorc"].Value, System.Globalization.CultureInfo.InvariantCulture);
                    _det.MueStock = grw.Cells["MueStock"].Value.ToString();
                    _det.Nota1 = grw.Cells["Nota1"].Value.ToString();
                    _det.Nota2 = grw.Cells["Nota2"].Value.ToString();
                    _det.Precio = grw.Cells["Precio"].Value == DBNull.Value ? 0 : Convert.ToDecimal(grw.Cells["Precio"].Value, System.Globalization.CultureInfo.InvariantCulture);
                    _det.Preciome = grw.Cells["Preciome"].Value == DBNull.Value ? 0 : Convert.ToDecimal(grw.Cells["Preciome"].Value, System.Globalization.CultureInfo.InvariantCulture);
                    _det.Proveedor = _proveedorPK;
                    _det.Talle = grw.Cells["Talle"].Value.ToString();
                    _det.Valor = grw.Cells["ValorDetalle"].Value == DBNull.Value ? 0 : Convert.ToDecimal(grw.Cells["ValorDetalle"].Value, System.Globalization.CultureInfo.InvariantCulture);
                    
                    _lstDetalle.Add(_det);

                    //Pagos.
                    var _pago = _lstPago.Find(ch => ch.Comprobante == _comprobante);
                    if (_pago == null)
                    {
                        CargoCheques(_comprobante, grw, _lstPago);
                    }

                    //cliente.
                    var _cliente = _lstCliente.Find(cl => cl.comprobante == _comprobante);
                    if (_cliente == null)
                    {
                        CargoCliente(_comprobante, grw, _lstCliente);
                    }

                }

                //Recorremos las cabeceras de los comprobantes
                foreach(Cabecera _cb in _lstCabecera)
                {
                    try
                    {
                        //Cargo la cabecera.
                        bsmService.clsCabecera _conta = CargoWsCabecera(_cb);
                        //busco los detalles.
                        var _det = _lstDetalle.Where(d => d.comprobante == _cb.Comprobante);
                        if(_det != null)
                        {
                            bsmService.clsDetalle[] _detalle = CargoWsDetalles(_det);

                            //Busco los pagos.
                            var _pago = _lstPago.Where(p => p.Comprobante == _cb.Comprobante);
                            if (_pago != null)
                            {
                                bsmService.clsCheque[] _cheques = CargoWsCheques(_pago);

                                //Busco el cliente.
                                var _cli = _lstCliente.Where(c => c.comprobante == _cb.Comprobante);
                                if (_cli != null)
                                {
                                    bsmService.clsCliente[] _cliente = CargoWsClientes(_cli);

                                    //Todo Ok.
                                    using (bsmService.PRKWebServiceSoapClient _ws = new PRKWebServiceSoapClient())
                                    {
                                        _ws.Endpoint.Address = new EndpointAddress(_urlWs);
                                        bool _rtaWs = _ws.RecibirDatosSucursal(_conta, _detalle, _cheques, _cliente);
                                        if (_rtaWs)
                                        {
                                            var _rw = grExcel.GetRows().Where(r => r.Cells[0].Value.ToString() == _conta.Comprobante);

                                            foreach (Janus.Windows.GridEX.GridEXRow pp in _rw)
                                            {
                                                Janus.Windows.GridEX.GridEXFormatStyle _style = new Janus.Windows.GridEX.GridEXFormatStyle();
                                                _style.BackColor = Color.Green;
                                                _style.ForeColor = Color.White;
                                                pp.RowStyle = _style;
                                            }
                                        }
                                        else
                                        {
                                            var _rw = grExcel.GetRows().Where(r => r.Cells[0].Value.ToString() == _conta.Comprobante);

                                            foreach (Janus.Windows.GridEX.GridEXRow pp in _rw)
                                            {
                                                Janus.Windows.GridEX.GridEXFormatStyle _style = new Janus.Windows.GridEX.GridEXFormatStyle();
                                                _style.BackColor = Color.Red;
                                                _style.ForeColor = Color.White;
                                                pp.RowStyle = _style;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    throw new Exception("El comprobante " + _cb.Comprobante + " no posee Cliente.");
                                }
                            }
                            else
                            {
                                throw new Exception("El comprobante " + _cb.Comprobante + " no posee Pagos.");
                            }
                        }
                        else
                        {
                            throw new Exception("El comprobante " + _cb.Comprobante + " no posee items.");
                        }
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show("Se produjo el siguiente error: " + ex.Message);
                    }
                }                
            }
            catch(Exception ex)
            {
                MessageBox.Show("Se produjo el siguiente Error: " + ex.Message);
            }
        }

        private void CargoEnvio(GridEXRow grw, string comprobante, List<Detalle> lstDet)
        {
            try
            {
                //Detalle.               
                Detalle detDto = new Detalle();
                detDto.comprobante = comprobante;
                detDto.Articulo = _skuEnvio;
                detDto.Cantidad = 1;
                detDto.ClaseArt = "Flete Pos";
                detDto.Color = "SIN COLOR";
                detDto.Columna = 6;
                detDto.CondPago = 100;
                detDto.Costo = Convert.ToDecimal(_costoPK, System.Globalization.CultureInfo.InvariantCulture);
                detDto.Descrip = "Envio Andreani";
                detDto.Dto1 = 0;
                detDto.Dto2 = 0;
                detDto.Dto3 = 0;
                detDto.Indice = _indiceDetallePK;
                detDto.IvaPorc = 21;
                detDto.MueStock = "R";
                detDto.Nota1 = "";
                detDto.Nota2 = "";
                detDto.Precio = Convert.ToDecimal(grw.Cells["Envio"].Value, System.Globalization.CultureInfo.InvariantCulture);
                detDto.Preciome = Convert.ToDecimal(grw.Cells["Envio"].Value, System.Globalization.CultureInfo.InvariantCulture);
                detDto.Proveedor = _proveedorPK;
                detDto.Talle = "UN";
                detDto.Valor = 1;

                lstDet.Add(detDto);
            }
            catch (Exception ex)
            {
                throw new Exception("Error el realizar el item descuento en el comprobante: " + comprobante);
            }
        }

        private void CargoDescuento(GridEXRow grw, string comprobante, List<Detalle> det)
        {
            try
            {
                //Detalle.               
                Detalle detDto = new Detalle();
                detDto.comprobante = comprobante;
                detDto.Articulo = _skuDescuento;
                detDto.Cantidad = -1;
                detDto.ClaseArt = "POSDTO";
                detDto.Color = "SIN COLOR";
                detDto.Columna = 6;
                detDto.CondPago = 100;
                detDto.Costo = Convert.ToDecimal(_costoPK, System.Globalization.CultureInfo.InvariantCulture);
                detDto.Descrip = "Descuento";
                detDto.Dto1 = 0;
                detDto.Dto2 = 0;
                detDto.Dto3 = 0;
                detDto.Indice = _indiceDetallePK;
                detDto.IvaPorc = 21;
                detDto.MueStock = "R";
                detDto.Nota1 = "";
                detDto.Nota2 = "";
                detDto.Precio = Convert.ToDecimal(grw.Cells["Descuento"].Value, System.Globalization.CultureInfo.InvariantCulture);
                detDto.Preciome = Convert.ToDecimal(grw.Cells["Descuento"].Value, System.Globalization.CultureInfo.InvariantCulture);
                detDto.Proveedor = _proveedorPK;
                detDto.Talle = "UN";
                detDto.Valor = 1;

                det.Add(detDto);
            }
            catch (Exception ex)
            {
                throw new Exception("Error el realizar el item descuento en el comprobante: " + comprobante);
            }
        }

        private clsCliente[] CargoWsClientes(IEnumerable<Cliente> cli)
        {
            clsCliente[] _arr = new clsCliente[cli.Count()];

            int counter = 0;

            try
            {
                foreach(Cliente _cl in cli)
                {
                    clsCliente cliente = new clsCliente()
                    {
                        Actualizar = _cl.Actualizar,
                        ApellidoEmp = _cl.ApellidoEmp,
                        Barrio = _cl.Barrio,
                        Codigo = _cl.Codigo,
                        Cliente = _cl.ClienteId,
                        Condpago = _cl.Condpago,
                        Cpostal = _cl.Cpostal,
                        Cuit = _cl.Cuit,
                        Desabili = _cl.Desabili,
                        Domicilio = _cl.Domicilio,
                        Email = _cl.Email,
                        EmailEmp = _cl.EmailEmp,
                        Fax = _cl.Fax,
                        Fechaalta = _cl.Fechaalta,
                        FecumpleEmp = _cl.FecumpleEmp,
                        Formapago = _cl.Formapago,
                        Nombre = _cl.Nombre,
                        NombreEmp = _cl.NombreEmp,
                        NombreFantasia = _cl.NombreFantasia,
                        NroIIBB = _cl.NroIIBB,
                        Postal = _cl.Postal,
                        Provincia = _cl.Provincia,
                        SexoEmp = _cl.SexoEmp,
                        Sucursal = _cl.Sucursal,
                        Telefono1 = _cl.Telefono1,
                        Telefono2 = _cl.Telefono2,
                        Telefono3 = _cl.Telefono3,
                        Tipodoc = _cl.Tipodoc,
                        Tipoiva = _cl.Tipoiva,
                        Vendedor = _cl.Vendedor,
                    };

                    _arr[counter] = cliente;

                    counter++;
                }
            }
            catch(Exception ex)
            {
                throw new Exception("Se produjo el siguiente error al procesar los Clientes. Error: " + ex.Message);
            }

            return _arr;
        }

        private clsCheque[] CargoWsCheques(IEnumerable<Pago> pago)
        {
            clsCheque[] _arr = new clsCheque[pago.Count()];

            int counter = 0;

            try
            {
                foreach (Pago pg in pago)
                {
                    clsCheque ch = new clsCheque()
                    {
                        Cheque = pg.Cheque,
                        Codigo = pg.Codigo,
                        Comprobante = pg.Comprobante,
                        Importe = pg.Importe,
                        ImporteMe = pg.ImporteMe,
                        Indice = pg.Indice,
                        MueCtaCte = pg.MueCtaCte,
                        Numero = pg.Numero,
                        SubPrograma = pg.SubPrograma,
                        CondPago = pg.CondPago,
                        Sucursal = pg.Sucursal,
                        Valor = pg.Valor,
                    };

                    _arr[counter] = ch;
                    counter++;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Se produjo el siguiente error al procesar los cheques. Error: " + ex.Message);
            }

            return _arr;
        }

        private clsDetalle[] CargoWsDetalles(IEnumerable<Detalle> det)
        {
            clsDetalle[] _arr = new clsDetalle[det.Count()];

            int counter = 0;

            try
            {
                foreach (Detalle pp in det)
                {
                    clsDetalle dt = new clsDetalle();
                    dt.Articulo = pp.Articulo;
                    dt.Cantidad = pp.Cantidad;
                    dt.ClaseArt = pp.ClaseArt;
                    dt.Color = pp.Color;
                    dt.Columna = pp.Columna;
                    dt.CondPago = pp.CondPago;
                    dt.Costo = pp.Costo;
                    dt.Descrip = pp.Descrip;
                    dt.Dto1 = pp.Dto1;
                    dt.Dto2 = pp.Dto2;
                    dt.Dto3 = pp.Dto3;
                    dt.Indice = pp.Indice;
                    dt.IvaPorc = pp.IvaPorc;
                    dt.MueStock = pp.MueStock;
                    dt.Nota1 = pp.Nota1;
                    dt.Nota2 = pp.Nota2;
                    dt.Precio = pp.Precio;
                    dt.Preciome = pp.Preciome;
                    dt.Proveedor = pp.Proveedor;
                    dt.Talle = pp.Talle;
                    dt.Valor = pp.Valor;

                    _arr[counter] = dt;

                    counter++;
                }
            }
            catch(Exception ex)
            {
                throw new Exception("Se produjo el siguiente error al procesar los detalles. Error: " + ex.Message);
            }

            return _arr;
        }

        private bsmService.clsCabecera CargoWsCabecera(Cabecera grw)
        {
            bsmService.clsCabecera _cabecera = new bsmService.clsCabecera();
            try
            {
                _cabecera.Anulado = grw.Anulado;
                _cabecera.Bruto = grw.Bruto;
                _cabecera.CliPro = grw.CliPro;
                _cabecera.Codigo = grw.Codigo;
                _cabecera.Comprobante = grw.Comprobante;
                _cabecera.CondPago = grw.CondPago;
                _cabecera.Empresa = grw.Empresa;
                _cabecera.Fecha = grw.Fecha;
                _cabecera.FechaCarga = grw.FechaCarga;
                _cabecera.FormaPago = grw.FormaPago;
                _cabecera.HoraOrigen = grw.HoraOrigen;
                _cabecera.Indice = grw.Indice;
                _cabecera.MueCtaCte = grw.MueCtaCte;
                _cabecera.Neto = grw.Neto;
                _cabecera.OtrosTax = grw.OtrosTax;
                _cabecera.RecargoDto2 = grw.RecargoDto2;
                _cabecera.RecargoDto3 = grw.RecargoDto3;
                _cabecera.Remito = grw.Remito;
                _cabecera.Sistema = grw.Sistema;
                _cabecera.SubPrograma = grw.SubPrograma;
                _cabecera.SucDesdeClase = grw.SucDesdeClase;
                _cabecera.SucHastaClase = grw.SucHastaClase;
                _cabecera.Sucursal = grw.Sucursal;
                _cabecera.SucursalF = grw.SucursalF;
                _cabecera.Turno = grw.Turno;
                _cabecera.UsuarioOrigen = grw.UsuarioOrigen;
                _cabecera.UsuarioPed = grw.UsuarioPed;
                _cabecera.Valor = grw.Valor;
                _cabecera.Vendedor = grw.Vendedor;

                //pCabeceraOk = true;
            }
            catch(Exception ex)
            {
                //pCabeceraOk = false;

                throw new Exception("Se produjo el siguiente error al cargar la Cabecera: " + ex.Message + ". Para el comprobante: " + grw.Comprobante);
            }
            return _cabecera;
        }

        private void CargoCabecera(Janus.Windows.GridEX.GridEXRow grw, string pComprobante, List<Cabecera> _cab)
        {
            Cabecera _cabecera = new Cabecera();
            try
            {
                _cabecera.Anulado = grw.Cells["Anulado"].Value.ToString();
                _cabecera.Bruto = Convert.ToDecimal(grw.Cells["Bruto"].Value, System.Globalization.CultureInfo.InvariantCulture);
                _cabecera.CliPro = Convert.ToInt32(_clientePK);
                _cabecera.Codigo = grw.Cells["Codigo"].Value.ToString();
                _cabecera.Comprobante = pComprobante;
                _cabecera.CondPago = Convert.ToInt32(grw.Cells["CondPago"].Value);
                _cabecera.Empresa = Convert.ToInt32(_empresaPK);
                _cabecera.Fecha = DateTime.Parse(grw.Cells["Fecha"].Value.ToString());
                //_cabecera.FechaCarga = DateTime.Parse(grw.Cells["FechaCarga"].Value.ToString());
                _cabecera.FechaCarga = DateTime.Parse(grw.Cells["Fecha"].Value.ToString());
                _cabecera.FormaPago = Convert.ToInt32(grw.Cells["FormaPago"].Value);
                _cabecera.HoraOrigen = _horaOrigenPK;
                _cabecera.Indice = grw.Cells["Indice"].Value.ToString();
                _cabecera.MueCtaCte = grw.Cells["MueCtaCte"].Value.ToString();
                _cabecera.Neto = Convert.ToDecimal(grw.Cells["Neto"].Value, System.Globalization.CultureInfo.InvariantCulture);
                _cabecera.OtrosTax = grw.Cells["OtrosTax"].Value.ToString();
                _cabecera.RecargoDto2 = grw.Cells["RecargoDto2"].Value == DBNull.Value ? 0 : Convert.ToDecimal(grw.Cells["RecargoDto2"].Value, System.Globalization.CultureInfo.InvariantCulture);
                _cabecera.RecargoDto3 = Convert.ToDecimal(grw.Cells["RecargoDto3"].Value, System.Globalization.CultureInfo.InvariantCulture);
                _cabecera.Remito = grw.Cells["Remito"].Value.ToString();
                //_cabecera.Sistema = grw.Cells["Sistema"].Value.ToString();
                _cabecera.Sistema = _sistemaPK;
                _cabecera.SubPrograma = grw.Cells["Subprograma"].Value.ToString();
                _cabecera.SucDesdeClase = grw.Cells["SucDesdeClase"].Value.ToString();
                //_cabecera.SucHastaClase = grw.Cells["SucHastaClase"].Value.ToString();
                _cabecera.SucHastaClase = _sucHastaClasePK;
                //_cabecera.Sucursal = Convert.ToInt32(grw.Cells["Sucursal"].Value);
                _cabecera.Sucursal = Convert.ToInt32(_sucursalPK);
                //_cabecera.SucursalF = Convert.ToInt32(grw.Cells["SucursalIF"].Value);
                //_cabecera.Turno = Convert.ToInt32(grw.Cells["Turno"].Value);
                _cabecera.Turno = Convert.ToInt32(_turnoPK);
                //_cabecera.UsuarioOrigen = grw.Cells["UsuarioOrigen"].Value.ToString();
                _cabecera.UsuarioOrigen = _usuarioOrigenPK;
                //_cabecera.UsuarioPed = grw.Cells["UsuarioPed"].Value.ToString();
                _cabecera.UsuarioPed = _usuarioPedPK;
                _cabecera.Valor = Convert.ToDecimal(grw.Cells["Valor"].Value, System.Globalization.CultureInfo.InvariantCulture);
                //_cabecera.Vendedor = Convert.ToInt32(grw.Cells["Vendedor"].Value);
                _cabecera.Vendedor = Convert.ToInt32(_vendedorPK);

                _cab.Add(_cabecera);
            }
            catch (Exception ex)
            {
                throw new Exception("Se produjo el siguiente error al cargar la Cabecera: " + ex.Message + ". Para el comprobante: " + pComprobante);
            }            
        }      

        private void CargoCheques(string pComprobante, Janus.Windows.GridEX.GridEXRow grw, List<Pago> _lstCheques)
        {
            Pago _cheque = new Pago();
            try
            {
                //_cheque.Cheque = grw.Cells["Cheque"].Value.ToString();
                _cheque.Cheque = _chequePK;
                _cheque.Codigo = grw.Cells["CodigoCheque"].Value.ToString();
                _cheque.Comprobante = pComprobante;
                _cheque.CondPago = grw.Cells["CondPagoCheque"].Value.ToString();
                _cheque.Importe = Convert.ToDecimal(grw.Cells["Importe"].Value, System.Globalization.CultureInfo.InvariantCulture);
                _cheque.ImporteMe = Convert.ToDecimal(grw.Cells["ImporteMe"].Value, System.Globalization.CultureInfo.InvariantCulture);
                //_cheque.Indice = grw.Cells["IndiceCheque"].Value.ToString();
                _cheque.Indice = _indiceChequePK;
                _cheque.MueCtaCte = grw.Cells["MueCtaCteCheque"].Value.ToString();
                _cheque.Numero = grw.Cells["Numero"].Value.ToString();
                _cheque.SubPrograma = grw.Cells["SubPrograma"].Value.ToString();
                if(grw.Cells["Codigo"].Value.ToString() == "CVL")
                    _cheque.Sucursal = Convert.ToInt32(_sucursalChequeCvlPK);
                else
                    _cheque.Sucursal = Convert.ToInt32(_sucursalChequePK);
                _cheque.Valor = Convert.ToDecimal(_valorChequePK, System.Globalization.CultureInfo.InvariantCulture);
                _lstCheques.Add(_cheque);
            }
            catch(Exception ex)
            {
                throw new Exception("Se produjo el siguiente error al cargar los Cheques: " + ex.Message + ". Para el comprobante: " + pComprobante);
            }
        }

        private void CargoCliente(string pComprobante, Janus.Windows.GridEX.GridEXRow grw, List<Cliente> _lstCliente)
        {
            Cliente _cliente = new Cliente();
            try
            {
                //_cliente.Actualizar = grw.Cells["Actualizar"].Value.ToString();
                _cliente.Actualizar = _actualizaClientePK;
                //_cliente.ApellidoEmp = grw.Cells["personaApe"].Value.ToString();
                _cliente.ApellidoEmp = _personaApeClientePK;
                //_cliente.Barrio = grw.Cells["Barrio"].Value.ToString();
                _cliente.Barrio = "";
                //_cliente.ClienteId = Convert.ToInt32(grw.Cells["Cliente"].Value);
                _cliente.ClienteId = Convert.ToInt32(_clientePK);
                //_cliente.Codigo = grw.Cells["CodigoCli"].Value.ToString();
                _cliente.Codigo = _codigoClientePK;
                _cliente.comprobante = pComprobante;
                //_cliente.Condpago = Convert.ToInt32(grw.Cells["CondPagoCliente"].Value);
                _cliente.Condpago = Convert.ToInt32(_condPagoClientePK);
                //_cliente.Cpostal = grw.Cells["CPostal"].Value == DBNull.Value ? "" : grw.Cells["CPostal"].Value.ToString();
                _cliente.Cpostal = _cPostalClientePK;
                //_cliente.Cuit = grw.Cells["CUIT"].Value.ToString();
                _cliente.Cuit = _cuitClientePK;
                //_cliente.Desabili = Convert.ToBoolean(grw.Cells["Desabili"].Value);
                //_cliente.Domicilio = grw.Cells["Domicilio"].Value.ToString();
                _cliente.Domicilio = _domicilioClientePK;
                //_cliente.Email = grw.Cells["Email"].Value == DBNull.Value ? "" : grw.Cells["Email"].Value.ToString();
                _cliente.Email = "";
                //_cliente.EmailEmp = grw.Cells["personaMail"].Value == DBNull.Value ? "" : grw.Cells["personaMail"].Value.ToString();
                _cliente.EmailEmp = "";
                //_cliente.Fax = grw.Cells["Fax"].Value == DBNull.Value ? "" : grw.Cells["Fax"].Value.ToString();
                _cliente.Fax = "";
                //_cliente.Fechaalta = Convert.ToDateTime(grw.Cells["FechaAlta"].Value);
                _cliente.Fechaalta = Convert.ToDateTime(_fechaClientePK);
                //_cliente.FecumpleEmp = grw.Cells["personaFCum"].Value == DBNull.Value ? DateTime.Now : Convert.ToDateTime(grw.Cells["personaFCum"].Value);
                _cliente.FecumpleEmp = DateTime.Now;
                //_cliente.Formapago = Convert.ToInt32(grw.Cells["FormaPagoCliente"].Value);
                _cliente.Formapago = Convert.ToInt32(_formaPagoClientePK);
                //_cliente.Nombre = grw.Cells["Nombre"].Value.ToString();
                _cliente.Nombre = _nombreClientePK;
                //_cliente.NombreEmp = grw.Cells["personaNom"].Value.ToString();
                _cliente.NombreEmp = _personaNomClientePK;
                //_cliente.NombreFantasia = grw.Cells["nomFantasia"].Value == DBNull.Value ? "": grw.Cells["nomFantasia"].Value.ToString();
                _cliente.NombreFantasia = _personaNomClientePK;
                //_cliente.NroIIBB = grw.Cells["nroingbrutos"].Value == DBNull.Value ? "" : grw.Cells["nroingbrutos"].Value.ToString();
                _cliente.NroIIBB = "";
                //_cliente.Postal = Convert.ToInt32(grw.Cells["postal"].Value);
                _cliente.Postal = Convert.ToInt32(_postalClientePK);
                //_cliente.Provincia = grw.Cells["provincia"].Value.ToString();
                _cliente.Provincia = _provinciaClientePK;
                //_cliente.SexoEmp = grw.Cells["personaGene"].Value.ToString();
                _cliente.SexoEmp = _personaGeneClientePK;
                //_cliente.Sucursal = Convert.ToInt32(grw.Cells["sucursalCli"].Value);
                _cliente.Sucursal = Convert.ToInt32(_sucursalPK);
                //_cliente.Telefono1 = grw.Cells["telefono1"].Value == DBNull.Value ? "" : grw.Cells["telefono1"].Value.ToString();
                _cliente.Telefono1 = "";
                //_cliente.Telefono2 = grw.Cells["telefono2"].Value == DBNull.Value ? "" : grw.Cells["telefono2"].Value.ToString();
                _cliente.Telefono2 = "";
                //_cliente.Telefono3 = grw.Cells["telefono3"].Value == DBNull.Value ? "" : grw.Cells["telefono3"].Value.ToString();
                _cliente.Telefono3 = "";
                //_cliente.Tipodoc = grw.Cells["tipodoc"].Value.ToString();
                _cliente.Tipodoc = _tipoDocClientePK;
                //_cliente.Tipoiva = grw.Cells["tipoiva"].Value.ToString();
                _cliente.Tipoiva = _tipoIvaClientePK;
                //_cliente.Vendedor = Convert.ToInt32(grw.Cells["vendedor"].Value);
                _cliente.Vendedor = Convert.ToInt32(_vendedorClientePK);
                _lstCliente.Add(_cliente);
            }
            catch(Exception ex)
            {
                throw new Exception("Se produjo el siguiente error al cargar el Cliente: " + ex.Message + ". Para el comprobante: " + pComprobante);
            }
        }
    }
}
