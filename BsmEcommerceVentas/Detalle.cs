﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BsmEcommerceVentas
{
    public class Detalle
    {
        public string comprobante { get; set; }
        /// <summary>
        /// Articulo del detalle.
        /// </summary>
        public string Articulo
        {
            get;
            set;
        }
        /// <summary>
        /// Cantidad del articulo.
        /// </summary>
        public decimal Cantidad
        {
            get;
            set;
        }
        /// <summary>
        /// Precio moneda extranjera.
        /// </summary>
        public decimal Preciome
        {
            get;
            set;
        }
        /// <summary>
        /// Precio del articulo.
        /// </summary>
        public decimal Precio
        {
            get;
            set;
        }
        /// <summary>
        /// Costo del articulo.
        /// </summary>
        public decimal Costo
        {
            get;
            set;
        }
        /// <summary>
        /// Indice del precio.
        /// </summary>
        public string Indice
        {
            get;
            set;
        }
        /// <summary>
        /// Valor del indice.
        /// </summary>
        public decimal Valor
        {
            get;
            set;
        }
        /// <summary>
        /// Columna de la lista de precio utilizada en la operacion.
        /// </summary>
        public int Columna
        {
            get;
            set;
        }
        /// <summary>
        /// Descuento 1
        /// </summary>
        public decimal Dto1
        {
            get;
            set;
        }
        /// <summary>
        /// Descuento 2
        /// </summary>
        public decimal Dto2
        {
            get;
            set;
        }
        /// <summary>
        /// Descuento 3
        /// </summary>
        public decimal Dto3
        {
            get;
            set;
        }

        /// <summary>
        /// Porcentual de IVA.
        /// </summary>
        public decimal IvaPorc
        {
            get;
            set;
        }
        /// <summary>
        /// Condicion de PAgo.
        /// </summary>
        public int CondPago
        {
            get;
            set;
        }
        /// <summary>
        /// Valor que indica si el articulo mueve o no Stock y como lo mueve.
        /// Si no posee valor, no mueve stock.
        /// Si el valor es S implica que suma Stock.
        /// Si el valor es R implica que resta Stock.
        /// </summary>
        public string MueStock
        {
            get;
            set;
        }
        /// <summary>
        /// Descripcion del articulo.
        /// </summary>
        public string Descrip
        {
            get;
            set;
        }
        /// <summary>
        /// Nota libre 1
        /// </summary>
        public string Nota1
        {
            get;
            set;
        }
        /// <summary>
        /// Nota libre 2
        /// </summary>
        public string Nota2
        {
            get;
            set;
        }
        /// <summary>
        /// Proveedor del articulo.
        /// </summary>
        public string Proveedor
        {
            get;
            set;
        }
        /// <summary>
        /// Clasee del articulo.
        /// </summary>
        public string ClaseArt
        {
            get;
            set;
        }
        /// <summary>
        /// Talle del articulo
        /// </summary>
        public string Talle
        {
            get;
            set;
        }
        /// <summary>
        /// Color del articulo.
        /// </summary>
        public string Color
        {
            get;
            set;
        }
    }
}
