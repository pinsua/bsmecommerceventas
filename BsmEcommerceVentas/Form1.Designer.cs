﻿namespace BsmEcommerceVentas
{
    partial class frmMain
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.grbExcel = new System.Windows.Forms.GroupBox();
            this.btnImportar = new System.Windows.Forms.Button();
            this.txtHoja = new System.Windows.Forms.TextBox();
            this.lblHoja = new System.Windows.Forms.Label();
            this.btnBuscarExcel = new System.Windows.Forms.Button();
            this.txtArchivo = new System.Windows.Forms.TextBox();
            this.lblArchivo = new System.Windows.Forms.Label();
            this.pnlIngresar = new System.Windows.Forms.Panel();
            this.btnIngresarSubfolder = new System.Windows.Forms.Button();
            this.grExcel = new Janus.Windows.GridEX.GridEX();
            this.dts = new System.Data.DataSet();
            this.grbExcel.SuspendLayout();
            this.pnlIngresar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grExcel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dts)).BeginInit();
            this.SuspendLayout();
            // 
            // grbExcel
            // 
            this.grbExcel.Controls.Add(this.btnImportar);
            this.grbExcel.Controls.Add(this.txtHoja);
            this.grbExcel.Controls.Add(this.lblHoja);
            this.grbExcel.Controls.Add(this.btnBuscarExcel);
            this.grbExcel.Controls.Add(this.txtArchivo);
            this.grbExcel.Controls.Add(this.lblArchivo);
            this.grbExcel.Dock = System.Windows.Forms.DockStyle.Top;
            this.grbExcel.Location = new System.Drawing.Point(0, 0);
            this.grbExcel.Name = "grbExcel";
            this.grbExcel.Size = new System.Drawing.Size(1044, 88);
            this.grbExcel.TabIndex = 2;
            this.grbExcel.TabStop = false;
            this.grbExcel.Text = "Seleccione el archivo Excel";
            // 
            // btnImportar
            // 
            this.btnImportar.Location = new System.Drawing.Point(867, 18);
            this.btnImportar.Name = "btnImportar";
            this.btnImportar.Size = new System.Drawing.Size(75, 23);
            this.btnImportar.TabIndex = 5;
            this.btnImportar.Text = "&Importar";
            this.btnImportar.UseVisualStyleBackColor = true;
            this.btnImportar.Click += new System.EventHandler(this.btnImportar_Click);
            // 
            // txtHoja
            // 
            this.txtHoja.Location = new System.Drawing.Point(660, 20);
            this.txtHoja.Name = "txtHoja";
            this.txtHoja.Size = new System.Drawing.Size(170, 20);
            this.txtHoja.TabIndex = 4;
            // 
            // lblHoja
            // 
            this.lblHoja.AutoSize = true;
            this.lblHoja.Location = new System.Drawing.Point(619, 23);
            this.lblHoja.Name = "lblHoja";
            this.lblHoja.Size = new System.Drawing.Size(29, 13);
            this.lblHoja.TabIndex = 3;
            this.lblHoja.Text = "Hoja";
            // 
            // btnBuscarExcel
            // 
            this.btnBuscarExcel.Location = new System.Drawing.Point(507, 19);
            this.btnBuscarExcel.Name = "btnBuscarExcel";
            this.btnBuscarExcel.Size = new System.Drawing.Size(75, 23);
            this.btnBuscarExcel.TabIndex = 2;
            this.btnBuscarExcel.Text = "&Buscar";
            this.btnBuscarExcel.UseVisualStyleBackColor = true;
            this.btnBuscarExcel.Click += new System.EventHandler(this.btnBuscarExcel_Click);
            // 
            // txtArchivo
            // 
            this.txtArchivo.Location = new System.Drawing.Point(86, 21);
            this.txtArchivo.Name = "txtArchivo";
            this.txtArchivo.Size = new System.Drawing.Size(415, 20);
            this.txtArchivo.TabIndex = 1;
            // 
            // lblArchivo
            // 
            this.lblArchivo.AutoSize = true;
            this.lblArchivo.Location = new System.Drawing.Point(34, 24);
            this.lblArchivo.Name = "lblArchivo";
            this.lblArchivo.Size = new System.Drawing.Size(43, 13);
            this.lblArchivo.TabIndex = 0;
            this.lblArchivo.Text = "Archivo";
            // 
            // pnlIngresar
            // 
            this.pnlIngresar.Controls.Add(this.btnIngresarSubfolder);
            this.pnlIngresar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlIngresar.Location = new System.Drawing.Point(0, 671);
            this.pnlIngresar.Name = "pnlIngresar";
            this.pnlIngresar.Size = new System.Drawing.Size(1044, 36);
            this.pnlIngresar.TabIndex = 3;
            // 
            // btnIngresarSubfolder
            // 
            this.btnIngresarSubfolder.Location = new System.Drawing.Point(384, 6);
            this.btnIngresarSubfolder.Name = "btnIngresarSubfolder";
            this.btnIngresarSubfolder.Size = new System.Drawing.Size(114, 23);
            this.btnIngresarSubfolder.TabIndex = 0;
            this.btnIngresarSubfolder.Text = "Ingresar Datos";
            this.btnIngresarSubfolder.UseVisualStyleBackColor = true;
            this.btnIngresarSubfolder.Click += new System.EventHandler(this.btnIngresarSubfolder_Click);
            // 
            // grExcel
            // 
            this.grExcel.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grExcel.AlternatingColors = true;
            this.grExcel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grExcel.GroupByBoxVisible = false;
            this.grExcel.Location = new System.Drawing.Point(0, 88);
            this.grExcel.Name = "grExcel";
            this.grExcel.RecordNavigator = true;
            this.grExcel.Size = new System.Drawing.Size(1044, 583);
            this.grExcel.TabIndex = 4;
            // 
            // dts
            // 
            this.dts.DataSetName = "NewDataSet";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1044, 707);
            this.Controls.Add(this.grExcel);
            this.Controls.Add(this.pnlIngresar);
            this.Controls.Add(this.grbExcel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Interfaz de ingreso de Facturas eCommerce";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.grbExcel.ResumeLayout(false);
            this.grbExcel.PerformLayout();
            this.pnlIngresar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grExcel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dts)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbExcel;
        private System.Windows.Forms.Button btnImportar;
        private System.Windows.Forms.TextBox txtHoja;
        private System.Windows.Forms.Label lblHoja;
        private System.Windows.Forms.Button btnBuscarExcel;
        private System.Windows.Forms.TextBox txtArchivo;
        private System.Windows.Forms.Label lblArchivo;
        private System.Windows.Forms.Panel pnlIngresar;
        private System.Windows.Forms.Button btnIngresarSubfolder;
        private Janus.Windows.GridEX.GridEX grExcel;
        private System.Data.DataSet dts;
    }
}

