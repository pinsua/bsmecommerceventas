﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BsmEcommerceVentas
{
    public class Cliente
    {
        public string comprobante { get; set; }
        /// <summary>
        /// Identificador del cliente.
        /// </summary>
        public int ClienteId
        {
            get;
            set;
        }
        /// <summary>
        /// Nombre del Cliente.
        /// </summary>
        public string Nombre
        {
            get;
            set;
        }
        /// <summary>
        /// fecha de alta del cliente.
        /// </summary>
        public DateTime Fechaalta
        {
            get;
            set;
        }
        /// <summary>
        /// numero de la sucursal del cliente.
        /// </summary>
        public int Sucursal
        {
            get;
            set;
        }
        /// <summary>
        /// domicilio del Cliente.
        /// </summary>
        public string Domicilio
        {
            get;
            set;
        }
        /// <summary>
        /// Numero de codigo postal.
        /// </summary>
        public int Postal
        {
            get;
            set;
        }
        /// <summary>
        /// Nuevo codigo postal.
        /// </summary>
        public string Cpostal
        {
            get;
            set;
        }
        /// <summary>
        /// provincia del cliente.
        /// </summary>
        public string Provincia
        {
            get;
            set;
        }
        /// <summary>
        /// Barrio o localidad del cliente.
        /// </summary>
        public string Barrio
        {
            get;
            set;
        }
        /// <summary>
        /// telefono 1 del cliente.
        /// </summary>
        public string Telefono1
        {
            get;
            set;
        }
        /// <summary>
        /// telefono 2 del cliente.
        /// </summary>
        public string Telefono2
        {
            get;
            set;
        }
        /// <summary>
        /// telefono 3 del cliente.
        /// </summary>
        public string Telefono3
        {
            get;
            set;
        }
        /// <summary>
        /// fax del cliente.
        /// </summary>
        public string Fax
        {
            get;
            set;
        }
        /// <summary>
        /// Email del cliente.
        /// </summary>
        public string Email
        {
            get;
            set;
        }
        /// <summary>
        /// tipo de IVA del cliente.
        /// </summary>
        public string Tipoiva
        {
            get;
            set;
        }
        /// <summary>
        /// Condicion de pago del cliente.
        /// </summary>
        public int Condpago
        {
            get;
            set;
        }
        /// <summary>
        /// Tipo documento del cliente.
        /// </summary>
        public string Tipodoc
        {
            get;
            set;
        }
        /// <summary>
        /// Codigo de pago del cliente.
        /// </summary>
        public string Codigo
        {
            get;
            set;
        }
        /// <summary>
        /// Nro de vendedor del cliente.
        /// </summary>
        public int Vendedor
        {
            get;
            set;
        }
        /// <summary>
        /// Forma de PAgo del cliente.
        /// </summary>
        public int Formapago
        {
            get;
            set;
        }
        /// <summary>
        /// Cuit del cliente.
        /// </summary>
        public string Cuit
        {
            get;
            set;
        }
        /// <summary>
        /// Nro de IIBBB del cliente.
        /// </summary>
        public string NroIIBB
        {
            get;
            set;
        }
        /// <summary>
        /// Flag que indica si se debe actualizar.
        /// </summary>
        public string Actualizar
        {
            get;
            set;
        }
        /// <summary>
        /// Nombre de fantasia
        /// </summary>
        public string NombreFantasia
        {
            get;
            set;
        }
        /// <summary>
        /// Fecha de nacimiento.
        /// </summary>
        public DateTime FecumpleEmp
        {
            get;
            set;
        }
        /// <summary>
        /// sexo.
        /// </summary>
        public string SexoEmp
        {
            get;
            set;
        }
        /// <summary>
        /// Nombre.
        /// </summary>
        public string NombreEmp
        {
            get;
            set;
        }
        /// <summary>
        /// Apellido.
        /// </summary>
        public string ApellidoEmp
        {
            get;
            set;
        }
        /// <summary>
        /// Email personal.
        /// </summary>
        public string EmailEmp
        {
            get;
            set;
        }
        /// <summary>
        /// Indica si el cliente esta deshabilitado.
        /// </summary>
        public bool Desabili
        { get; set; }
    }
}
