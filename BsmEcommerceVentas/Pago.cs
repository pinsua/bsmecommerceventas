﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BsmEcommerceVentas
{
    public class Pago
    {

        /// <summary>
        /// Numero de Comprobante.
        /// </summary>
        public string Comprobante
        {
            get;
            set;
        }
        /// <summary>
        /// Importe.
        /// </summary>
        public decimal Importe
        {
            get;
            set;
        }
        /// <summary>
        /// Importe Moneda Extranjera.
        /// </summary>
        public decimal ImporteMe
        {
            get;
            set;
        }
        /// <summary>
        /// Identificador del Cheque.
        /// </summary>
        public string Cheque
        {
            get;
            set;
        }
        /// <summary>
        /// Cotizacion moneda extranjera.
        /// </summary>
        public decimal Valor
        {
            get;
            set;
        }
        /// <summary>
        /// Tipo de Moneda.
        /// </summary>
        public string Indice
        {
            get;
            set;
        }
        /// <summary>
        /// Numero de Sucursal.
        /// </summary>
        public int Sucursal
        {
            get;
            set;
        }
        /// <summary>
        /// Codigo de la forma de Pago.
        /// </summary>
        public string Codigo
        {
            get;
            set;
        }
        /// <summary>
        /// Indica como impacta el Cheque. (Suma o resta)
        /// </summary>
        public string MueCtaCte
        {
            get;
            set;
        }
        /// <summary>
        /// Numero de cheque/Pago.
        /// </summary>
        public string Numero
        {
            get;
            set;
        }
        /// <summary>
        /// Condición de Pago.
        /// </summary>
        public string CondPago
        {
            get;
            set;
        }
        /// <summary>
        /// Subprograma
        /// </summary>
        public string SubPrograma
        {
            get;
            set;
        }
    }
}
