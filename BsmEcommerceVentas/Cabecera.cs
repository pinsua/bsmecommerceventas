﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BsmEcommerceVentas
{
    public class Cabecera
    {
        /// <summary>
        /// Variable del identificador de la empresa.
        /// </summary>
        public int Empresa { get; set; }
        /// <summary>
        /// Variable de Sistema de la operacion.
        /// </summary>
        public string Sistema { get; set; }
        /// <summary>
        /// Variable con el Codigo del detalle.
        /// </summary>
        public string Codigo { get; set; }
        /// <summary>
        /// Variable del Comprobante de la operacion.
        /// </summary>
        public string Comprobante { get; set; }
        /// <summary>
        /// Variable del Turno de la operacion.
        /// </summary>
        public int Turno { get; set; }
        /// <summary>
        /// Variable de Fecha de la operacion.
        /// </summary>
        public DateTime Fecha { get; set; }
        /// <summary>
        /// Variable del vendedor de la operacion.
        /// </summary>
        public int Vendedor { get; set; }
        /// <summary>
        /// Variable de la sucursal de la operacion.
        /// </summary>
        public int Sucursal { get; set; }
        /// <summary>
        /// Cliente.
        /// </summary>
        public int CliPro { get; set; }
        /// <summary>
        /// Varialbe de la hora de la carga de la operacion.
        /// </summary>
        public string HoraOrigen { get; set; }
        /// <summary>
        /// Usuario que ingreso la operacion
        /// </summary>
        public string UsuarioOrigen { get; set; }
        /// <summary>
        /// Usuario que realizo el pedido.
        /// </summary>
        public string UsuarioPed { get; set; }
        /// <summary>
        /// Valor neto de la operacion.
        /// </summary>
        public decimal Neto { get; set; }
        /// <summary>
        /// Forma de pago de la operacion.
        /// </summary>
        public int FormaPago { get; set; }
        /// <summary>
        /// Valor
        /// </summary>
        public decimal Valor { get; set; }
        /// <summary>
        /// Indice
        /// </summary>
        public string Indice { get; set; }
        /// <summary>
        /// Fecha de carga de la operacion.
        /// </summary>
        public DateTime FechaCarga { get; set; }
        /// <summary>
        /// Importe Bruto del Ticket.
        /// </summary>
        public decimal Bruto { get; set; }
        /// <summary>
        /// Condicion de Pago.
        /// </summary>
        public int CondPago { get; set; }
        /// <summary>
        /// Numero del remito.
        /// </summary>
        public string Remito { get; set; }
        /// <summary>
        /// Fecha de la anulacion del formulario.
        /// </summary>
        public string Anulado { get; set; }
        /// <summary>
        /// Nro. de Sucursal Destino.
        /// </summary>
        public int SucursalF { get; set; }
        /// <summary>
        /// Recargo o Dto2.
        /// </summary>
        public decimal RecargoDto2 { get; set; }
        /// <summary>
        /// Recargo o dto3.
        /// </summary>
        public decimal RecargoDto3 { get; set; }
        /// <summary>
        /// Otros impuestos
        /// </summary>
        public string OtrosTax { get; set; }
        /// <summary>
        /// SubPrograma
        /// </summary>
        public string SubPrograma { get; set; }
        /// <summary>
        /// Clase de la sucursal desde.
        /// </summary>
        public string SucDesdeClase { get; set; }
        /// <summary>
        /// Clase de la sucursal Hasta.
        /// </summary>
        public string SucHastaClase { get; set; }
        /// <summary>
        /// Indica si mueve CtaCte.
        /// </summary>
        public string MueCtaCte { get; set; }
    }
}
